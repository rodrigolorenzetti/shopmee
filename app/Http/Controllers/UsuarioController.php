<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegisteredMail;
use App\Exports\UsuarioFromView;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        return view('usuario.all')->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (DB::table('usuarios')->where('email', $request->email)->count() == 0){ 
            $usuario = new Usuario();
            $usuario->email = $request->email;
            $usuario->nome = $request->nome;
            $usuario->save();
            return $this->registered($usuario);
        }else{
            return view('usuario.failure')->with('request', $request);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        return view('usuario.edit')->with('usuario', $usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parametros = $request->all();
        $usuario = Usuario::find($id);
        $usuario->update($parametros);
        $usuario->save();
        return redirect('usuario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();
        return redirect('usuario');
    }

    public function registered($usuario){
        Mail::to($usuario->email)->send(new UserRegisteredMail($usuario));
        return $this->success($usuario);
    }

    public function success($usuario){
        return view('usuario.success')->with('usuario', $usuario);
    }
    
    public function export(){
        if (DB::table('usuarios')->count() > 0){
            return Excel::download(new UsuarioFromView, 'usuarios.xlsx');
        }else{
            return redirect('usuario')->with('erro', 'Não é possível baixar a planilha sem usuários cadastrados');
        }
    }
}
