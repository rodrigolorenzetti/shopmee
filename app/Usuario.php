<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = ['method', 'nome', 'email'];
    protected $primaryKey = 'id';
}
