<?php

namespace App\Exports;

use App\Usuario;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsuarioFromView implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Usuario::all();
    }

    public function map($usuario): array{
        return [
            $usuario->id,
            $usuario->nome,
            $usuario->email
        ];
    }

    public function headings(): array{
        return[
            'ID',
            'Nome',
            'Email',
        ];
    }   
}
