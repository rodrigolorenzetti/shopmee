<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Usuario;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Usuario $usuario)
        {
            $this->usuario = $usuario;
        }
    
        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            $this->subject('Obrigado por se cadastrar!');
            return $this->markdown('email/user-registered')->with("usuario", $this->usuario);
        }
}
