<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('/css/create.css')}}">
    <link rel="shortcut icon" href="{{URL::asset('/image/favicon.ico')}}">
    <title>Editar Usuário</title>
</head>
<body>
    
    <div class="container col-lg-5 col-md-8 col-sm-10 shadow p-3 mt-5 mb-5">
        <div class="content">
            <div class="w-100">
                <img src="{{URL::asset('/image/brand.png')}}" class="mx-auto d-block brand-img" alt='shopmee brand'>
            </div>

            <p class="text-left mt-4">Olá <strong>{{$usuario->nome}}</strong>, insira os campos abaixo e seus dados serão editados.</p>

            <form action="{{route('usuario.update', $usuario->id)}}" method="POST" class="mt-5 needs-validation novalidate">
                @method('PUT')
                @csrf
                <div class="form-group w-100">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control p-3 ipn-type ipn" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" maxlength="64" placeholder="{{$usuario->email}}" required>
                </div>
                <div class="form-group mt-3">
                    <label for="email">Nome</label>
                    <input type="text" class="form-control p-3 ipn-type ipn" maxlength="64" name="nome" placeholder="{{$usuario->nome}}" required>
                  </div>
                <button class="btn w-100 ipn-type sbm-btn" type="submit">Editar</button>
            </form>
        </div>
    </div>

    
    {{-- Bootstrap --}}
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>