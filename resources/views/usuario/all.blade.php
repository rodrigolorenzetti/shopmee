<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('/css/all.css')}}">
    <link rel="shortcut icon" href="{{URL::asset('/image/favicon.ico')}}">
    <title>Usuários</title>
</head>
<body>
    
    <div class="container col-lg-5 col-md-8 col-sm-8 shadow p-3 mt-5 mb-5 bg-white">
        <div class="content">

            
            <div class="w-100">
                <div class="brand-div">
                    <img src="{{URL::asset('/image/brand.png')}}" class="align-left ml-3 brand-img" alt='shopmee brand'>
                </div>
                
                <div class="download-div text-right">
                    <a class="btn create align-right w-100" href="{{route('exportar')}}">
                        <img src="{{URL::asset('/image/excel.png')}}" class=" ml-3 download" alt='excel'>Download
                    </a>
                </div>
            </div>

            <div class="w-100">
                <h5 class="align-left mt-2 ml-3 text-secondary cadastred">Usuários cadastrados</h5>
                
                <a class="btn create" href="{{route('usuario.create')}}">
                    Criar Usuário
                </a>
               
            </div>

            <hr>

            @if (\Session::has('erro'))
                <h5 class="text-danger text-center">{!! \Session::get('erro') !!}</h5>
            @endif
                

            @foreach ($usuarios as $usuario)
            <div class="cards ml-3 mb-5">
                <div class="title w-100 shadow">
                    <p class="text-center">{{$usuario->nome}}</p>
                </div>

                <div class="card-content shadow">
                    <div class="nome mt-3 ml-3">
                        <p ><strong>Nome:</strong></p>
                        <ul>
                            <li>{{$usuario->nome}}</li>
                        </ul>
                    </div>

                    <div class="email mt-3 ml-3 mb-3">
                        <p ><strong>E-mail:</strong></p>
                        <ul>
                            <li>{{$usuario->email}}</li>
                        </ul>
                    </div>

                    <div class="buttons w-100 text-center">
                        <a class="btn edit mb-3" href="{{route('usuario.edit', $usuario->id)}}">
                            Editar
                        </a>
                        <form action="{{route('usuario.destroy', $usuario->id)}}" method="POST" class="destroy" onsubmit = 'return ConfirmDelete()'>
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger w-100 mb-3">Excluir</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


    {{-- Bootstrap --}}
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <script>
        function ConfirmDelete(){
            var x = confirm("Deseja realmente excluir o usuário?");
            if (x){
                return true;
            }else{
                return false;
            }
        }
    </script>
</body>
</html>