@component('mail::message')
<h1>Obrigado por se cadastrar, {{$usuario->nome}}</h1>

<h2>Abaixo estão os dados cadastrados por você:</h2>

<p><Strong>ID:</Strong> {{$usuario->id}}</p>
<p><Strong>Nome:</Strong> {{$usuario->nome}}</p>
<p><Strong>E-mail:</Strong> {{$usuario->email}}</p>

<h2>Atenciosamente, Shopmee.</h2>
    
@endcomponent

